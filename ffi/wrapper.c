#include <ffi_platypus_bundle.h>
#include <cxcregion.h>

#define insideObject( Object, Type )                                    \
void p5_cxcregion_regInside##Object##_##Type( reg##Object *obj, double* x, double* y, Type* yes, long n) { \
                                                                        \
    for( ; n ; --n, ++x, ++y, ++yes )                                   \
        *yes++ = regInside##Object( obj, *x++, *y++);                   \
}


insideObject(Shape,int)
insideObject(Shape,long)
insideObject(Shape,short)
insideObject(Shape,char)

insideObject( Region, int )
insideObject( Region, long )
insideObject( Region, short )
insideObject( Region, char )

