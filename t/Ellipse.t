#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Ellipse';

my $shape;

ok(
    lives {
        $shape = Ellipse->new( CIAO::Lib::Region::Include,
                               10, 8,
                               [ 33, 44 ],
                               55,
                              CIAO::Lib::Region::Physical,
                              CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [10,8], "correct position" );

my $radii;
ok( lives { $radii = $shape->radii }, "get radii" );
is ( $radii, [ 33, 44 ], "correct radii" );

my $angle;
ok( lives { $angle = $shape->angle }, "get angle" );
is ( $angle, 55, "correct angle" );

done_testing;
