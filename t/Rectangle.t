#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Rectangle';

my $shape;

ok(
    lives {
        $shape = Rectangle->new( CIAO::Lib::Region::Include,
                           [ 8, 10  ],
                           [ 11, 33 ],
                           44,
                           CIAO::Lib::Region::Physical,
                           CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->points }, "get points" );
is( $x, [8, 10], "x position" );
is( $y, [11,33], "y position" );

my $angle;
ok( lives { $angle = $shape->angle }, "get angle" );
is ( $angle, 44, "correct angle" );


done_testing;
