#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Circle';

my $shape;

ok(
    lives {
        $shape = Circle->new( CIAO::Lib::Region::Include, 10, 8, 33,
                              CIAO::Lib::Region::Physical,
                              CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [10,8], "correct position" );

my $radius;
ok( lives { $radius = $shape->radius }, "get radius" );
is ( $radius, 33, "correct radius" );

done_testing;
