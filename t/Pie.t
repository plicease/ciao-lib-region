#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Pie';

my $shape;

ok(
    lives {
        $shape = Pie->new( CIAO::Lib::Region::Include,
                           10, 8,
                           [ 11, 33 ],
                           [ 44, 55 ],
                           CIAO::Lib::Region::Physical, CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [10,8], "correct position" );

my $radii;
ok( lives { $radii = $shape->radii }, "get radii" );
is ( $radii, [11, 33], "correct radii" );

my $angles;
ok( lives { $angles = $shape->angles }, "get angles" );
is ( $angles, [44, 55], "correct angles" );

done_testing;
