#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Polygon';

my $shape;

ok(
    lives {
        $shape = Polygon->new( CIAO::Lib::Region::Include,
                           [10, 11, 12, 13 ],
                           [14, 15, 16, 17 ],
                           CIAO::Lib::Region::Physical,
                           CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->points }, "get points" );
is( $x, [10, 11, 12, 13, 10 ], "correct x" );
is( $y, [14, 15, 16, 17, 14 ], "correct y" );

done_testing;
