#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Point';

my $shape;

ok(
    lives {
        $shape = Point->new( CIAO::Lib::Region::Include, 10, 8,
            CIAO::Lib::Region::Physical, CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "retrieve points" );

is( [ $x, $y ], [10, 8], "correct position" );

done_testing;
