#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Field';

my $shape;

ok(
    lives {
        $shape = Field->new( CIAO::Lib::Region::Include,
                              CIAO::Lib::Region::Physical,
                              CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

done_testing;
