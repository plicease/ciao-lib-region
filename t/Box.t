#! perl
use Test2::V0;

use CIAO::Lib::Region;
use aliased 'CIAO::Lib::Region::Box';

my $shape;

ok(
    lives {
        $shape = Box->new( CIAO::Lib::Region::Include,
                           10, 8,
                           [ 11, 33 ],
                           44,
                           CIAO::Lib::Region::Physical,
                           CIAO::Lib::Region::Physical )
    },
    "construct object"
) or note $@;

my ( $x, $y );
ok( lives { ( $x, $y ) = $shape->point }, "get points" );
is( [ $x, $y ], [10,8], "correct position" );

my $sides;
ok( lives { $sides = $shape->sides }, "get sides" );
is ( $sides, [11, 33], "correct sides" );

my $angle;
ok( lives { $angle = $shape->angle }, "get angle" );
is ( $angle, 44, "correct angle" );

is( $shape->area, 363, 'area' );


done_testing;
