package CIAO::Lib::Region::Annulus;

# ABSTRACT: FFI support for Annulus shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Annulus)'   => 'Annulus_t' );
ffi->attach( [ regCreateAnnulus => 'new' ],
              # include,   xpos,     ypos,     radius,      wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'double[2]',   'int', 'int' ],
              'Annulus_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include,  $xpos,  $ypos, $radius, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, $radius, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;
*radii = \&CIAO::Lib::Region::Shape::radii;


ffi->attach( [ regCopyAnnulus       => 'copy'   ], [ 'Annulus_t' ] => 'Annulus_t' );
ffi->attach( [ regCalcAreaAnnulus   => 'area'   ], [ 'Annulus_t' ] =>  'double' );
ffi->attach( [ regCalcExtentAnnulus => 'extent' ], [ 'Annulus_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideAnnulus     => 'inside' ], [ 'Annulus_t', 'double', 'double' ] => 'bool' );

1;
