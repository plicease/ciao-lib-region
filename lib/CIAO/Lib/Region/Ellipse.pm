package CIAO::Lib::Region::Ellipse;

# ABSTRACT: FFI support for Ellipse shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Ellipse)'   => 'Ellipse_t' );
ffi->attach( [ regCreateEllipse => 'new' ],
              # include,   xpos,     ypos,     radius,      angle    wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'double[2]', 'double*', 'int', 'int' ],
              'Ellipse_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include,  $xpos,  $ypos, $radius,  $angle, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, $radius, \$angle, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;
*angle = \&CIAO::Lib::Region::Shape::angle;

ffi->attach( [ regCopyEllipse       => 'copy'   ], [ 'Ellipse_t' ] => 'Ellipse_t' );
ffi->attach( [ regCalcAreaEllipse   => 'area'   ], [ 'Ellipse_t' ] =>  'double' );
ffi->attach( [ regCalcExtentEllipse => 'extent' ], [ 'Ellipse_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideEllipse     => 'inside' ], [ 'Ellipse_t', 'double', 'double' ] => 'bool' );

1;
