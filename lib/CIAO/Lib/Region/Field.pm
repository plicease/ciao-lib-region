package CIAO::Lib::Region::Field;

# ABSTRACT: FFI support for Field shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Field)'   => 'Field_t' );
ffi->attach( [ regCreateField => 'new' ],
              # include,   wcoord,  wsize
              [ 'enum', 'int', 'int' ],
              'Field_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include, $wcoord, $wsize ) = @_;
                  return $sub->( $include, $wcoord, $wsize );
              }
            );

ffi->attach( [ regCopyField       => 'copy'   ], [ 'Field_t' ] => 'Field_t' );
ffi->attach( [ regCalcAreaField   => 'area'   ], [ 'Field_t' ] =>  'double' );
ffi->attach( [ regCalcExtentField => 'extent' ], [ 'Field_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideField     => 'inside' ], [ 'Field_t', 'double', 'double' ] => 'bool' );

1;
