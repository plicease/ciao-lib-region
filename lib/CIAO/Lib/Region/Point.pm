package CIAO::Lib::Region::Point;

# ABSTRACT: FFI support for Point shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Point)'   => 'Point_t' );
ffi->attach( [ regCreatePoint => 'new' ],
              # include,   xpos,     ypos,   wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'int', 'int' ],
              'Point_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include,  $xpos,  $ypos, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;

ffi->attach( [ regCopyPoint       => 'copy'   ], [ 'Point_t' ] => 'Point_t' );
ffi->attach( [ regCalcAreaPoint   => 'area'   ], [ 'Point_t' ] => 'double' );
ffi->attach( [ regCalcExtentPoint => 'extent' ], [ 'Point_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsidePoint     => 'inside' ], [ 'Point_t', 'double', 'double' ] => 'bool' );

# void regToStringPoint( regShape* shape, char* buf, long maxlen ) {

1;
