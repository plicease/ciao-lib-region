package CIAO::Lib::Region::Shape;

# ABSTRACT: FFI support for Generic Shapes

use strict;
use warnings;
our $VERSION = '0.01';

use strict;
use warnings;

use FFI::Platypus::Buffer qw( scalar_to_pointer );

use CIAO::Lib::Region;

use constant ffi => CIAO::Lib::Region::ffi();
use constant sizeof_double => ffi->sizeof('double');

# Shape Accessors

ffi->attach( [ regShapeGetName   => 'name'   ] => [ 'shape_t', 'string', 'long']            => 'int'
              => sub {
                  my ( $sub, $shape ) = @_;
                  my $len = 128;  # The longest shape name is "RotRectangle", but just to be safe...
                  my $buf = pack( '.', $len + 1 );
                  $sub->( $shape, $buf, $len );
                  return $buf;
              }
            );

ffi->attach( [ regShapeGetPoints => 'points' ] => [ 'shape_t', 'opaque', 'opaque', 'long' ] => 'long',
    sub {
	my ( $sub, $shape ) = @_;
	my $npts = $shape->num_points;

        # can't just assign $xp to $yp, as direct C manipulation of the buffer
        # avoids Perl's Copy-On-Write optimization.
        my $xp = pack( '.', $npts * sizeof_double );
        my $yp = pack( '.', $npts * sizeof_double );
        my $got = $sub->( $shape,
                          scalar_to_pointer($xp),
                          scalar_to_pointer($yp),
                          $npts );
        my $fmt = 'd' . $got;
        return map { [ unpack( $fmt, $_ ) ] } $xp, $yp;
    } );

ffi->attach( [ regShapeGetPoints => 'point' ],
             [ 'shape_t', 'double*', 'double*', 'long' ],
             'long',
             sub {
                 my ( $sub, $shape ) = @_;
                 my ($x,$y);
                 my $got = $sub->( $shape, \$x, \$y, 1 );
                 return ($x, $y );
             } );

ffi->attach( [ reg_shape_angles     => 'num_angles'  ] => [ 'shape_t' ] => 'long' );
ffi->attach( [ regShapeGetAngles    => 'angles'     ] => [ 'shape_t', 'opaque' ] => 'long'
              => sub {
                  my ( $sub, $shape ) = @_;
                  my $nangles = $shape->num_angles;
                  my $buf = pack( '.', $nangles * sizeof_double );
                  my $got = $sub->( $shape, scalar_to_pointer($buf), $nangles );
                  return [ unpack( 'd' . $got, $buf ) ];
              }
            );

ffi->attach( [ regShapeGetAngles    => 'angle'     ] => [ 'shape_t', 'double*' ] => 'long'
              => sub {
                  my ( $sub, $shape ) = @_;
                  my $angle;
                  my $got = $sub->( $shape, \$angle, 1 );
                  return $angle;
              }
            );

ffi->attach( [ reg_shape_radii      => 'num_radii'  ] => [ 'shape_t' ] => 'long' );
ffi->attach( [ regShapeGetRadii     => 'radii'      ] => [ 'shape_t', 'opaque'] => 'long'
              => sub {
                  my ( $sub, $shape ) = @_;
                  my $nradii = $shape->num_radii;
                  my $buf = pack( '.', $nradii * sizeof_double );
                  my $got = $sub->( $shape, scalar_to_pointer($buf), $nradii );
                  return [ unpack( 'd' . $got, $buf ) ];
              } );

ffi->attach( [ regShapeGetRadii     => 'radius'      ] => [ 'shape_t', 'double*'] => 'long'
              => sub {
                  my ( $sub, $shape ) = @_;
                  my $radius;
                  my $got = $sub->( $shape, \$radius, 1 );
                  return $radius;
              } );

ffi->attach( [ regShapeGetNoPoints  => 'num_points' ] => [ 'shape_t' ] => 'long' );
ffi->attach( [ regShapeGetComponent => 'component'  ] => [ 'shape_t' ] => 'long' );

# Shape Operations
ffi->attach( [ regCompareShape => 'compare' ] => [ 'shape_t', 'shape_t', 'short'] => 'int'     );
ffi->attach( [ regCopyShape    => 'copy'    ] => [ 'shape_t']                     => 'shape_t' );
ffi->attach( [ regInsideShape  => 'inside'  ] => [ 'shape_t', 'double', 'double'] => 'int'     );
ffi->attach( [ regPrintShape   => 'print'   ] => [ 'shape_t']                     => 'void'    );

1;
