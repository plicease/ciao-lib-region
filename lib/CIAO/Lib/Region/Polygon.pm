package CIAO::Lib::Region::Polygon;

# ABSTRACT: FFI support for Polygon shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Polygon)'   => 'Polygon_t' );
ffi->attach( [ regCreatePolygon => 'new' ],
              # include,   xpos,     ypos,     npoints, wcoord,  wsize
              [ 'enum', 'double[]', 'double[]', 'long',   'int', 'int' ],
              'Polygon_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include, $xpos, $ypos,        $wcoord, $wsize ) = @_;
                  my $npos = @$xpos;
                  die( "xpos and ypos must have same number of elements\n" )
                    unless $npos == @$ypos;
                  return $sub->( $include, $xpos, $ypos, $npos, $wcoord, $wsize );
              }
            );

ffi->attach( [ regCopyPolygon       => 'copy'   ], [ 'Polygon_t' ] => 'Polygon_t' );
ffi->attach( [ regCalcAreaPolygon   => 'area'   ], [ 'Polygon_t' ] =>  'double' );
ffi->attach( [ regCalcExtentPolygon => 'extent' ], [ 'Polygon_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsidePolygon     => 'inside' ], [ 'Polygon_t', 'double', 'double' ] => 'bool' );


1;
