package CIAO::Lib::Region::Sector;

# ABSTRACT: FFI support for Sector shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Sector)'   => 'Sector_t' );
ffi->attach( [ regCreateSector => 'new' ],
              # include,   xpos,     ypos,     angle,       wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'double[2]',   'int', 'int' ],
              'Sector_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include,  $xpos,  $ypos, $angle, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, $angle, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;

ffi->attach( [ regCopySector       => 'copy'   ], [ 'Sector_t' ] => 'Sector_t' );
ffi->attach( [ regCalcAreaSector   => 'area'   ], [ 'Sector_t' ] =>  'double' );
ffi->attach( [ regCalcExtentSector => 'extent' ], [ 'Sector_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideSector     => 'inside' ], [ 'Sector_t', 'double', 'double' ] => 'bool' );

1;
