package CIAO::Lib::Region::Rectangle;

# ABSTRACT: FFI support for Rectangle shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();


ffi->type('object(CIAO::Lib::Region::Rectangle)'   => 'Rectangle_t' );
ffi->attach( [ regCreateRectangle => 'new' ],
              # include,   xpos,        ypos,     angle,   wcoord,  wsize
              [ 'enum', 'double[2]', 'double[2]', 'double*', 'int', 'int' ],
              'Rectangle_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include, $xpos, $ypos,  $angle, $wcoord, $wsize ) = @_;
                  return $sub->( $include, $xpos, $ypos, \$angle, $wcoord, $wsize );
              }
            );

*angle  = \&CIAO::Lib::Region::Shape::angle;

ffi->attach( [ regCopyRectangle       => 'copy'   ], [ 'Rectangle_t' ] => 'Rectangle_t' );
ffi->attach( [ regCalcAreaRectangle   => 'area'   ], [ 'Rectangle_t' ] =>  'double' );
ffi->attach( [ regCalcExtentRectangle => 'extent' ], [ 'Rectangle_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideRectangle     => 'inside' ], [ 'Rectangle_t', 'double', 'double' ] => 'bool' );


