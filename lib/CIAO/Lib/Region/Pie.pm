package CIAO::Lib::Region::Pie;

# ABSTRACT: FFI support for Pie shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Pie)'   => 'Pie_t' );
ffi->attach( [ regCreatePie => 'new' ],
              # include,   xpos,     ypos,     radius,      angle,     wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'double[2]', 'double[2]', 'int', 'int' ],
              'Pie_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include , $xpos,  $ypos, $radius, $angle, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, $radius, $angle, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;

ffi->attach( [ regCopyPie       => 'copy'   ], [ 'Pie_t' ] => 'Pie_t' );
ffi->attach( [ regCalcAreaPie   => 'area'   ], [ 'Pie_t' ] =>  'double' );
ffi->attach( [ regCalcExtentPie => 'extent' ], [ 'Pie_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsidePie     => 'inside' ], [ 'Pie_t', 'double', 'double' ] => 'bool' );

1;
