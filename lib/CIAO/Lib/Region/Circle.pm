package CIAO::Lib::Region::Circle;

# ABSTRACT: FFI support for Circle shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Circle)'   => 'Circle_t' );
ffi->attach( [ regCreateCircle => 'new' ],
              # include,   xpos,     ypos,     radius,   wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'double*', 'int', 'int' ],
              'Circle_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include,  $xpos,  $ypos,  $radius, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, \$radius, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;
*radius = \&CIAO::Lib::Region::Shape::radius;


ffi->attach( [ regCopyCircle       => 'copy'   ], [ 'Circle_t' ] => 'Circle_t' );
ffi->attach( [ regCalcAreaCircle   => 'area'   ], [ 'Circle_t' ] =>  'double' );
ffi->attach( [ regCalcExtentCircle => 'extent' ], [ 'Circle_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideCircle     => 'inside' ], [ 'Circle_t', 'double', 'double' ] => 'bool' );

1;
