package CIAO::Lib::Region::Box;

# ABSTRACT: FFI support for Box shape

use strict;
use warnings;
our $VERSION = '0.01';

use parent 'CIAO::Lib::Region::Shape';

use constant ffi => CIAO::Lib::Region::ffi();

ffi->type('object(CIAO::Lib::Region::Box)'   => 'Box_t' );
ffi->attach( [ regCreateBox => 'new' ],
              # include,   xpos,     ypos,     radius[2],   angle,   wcoord,  wsize
              [ 'enum', 'double*', 'double*', 'double[2]', 'double*', 'int', 'int' ],
              'Box_t',
              sub {
                  my $sub = shift;
                  my $class = shift;
                  my (           $include,  $xpos,  $ypos, $radius,  $angle, $wcoord, $wsize ) = @_;
                  return $sub->( $include, \$xpos, \$ypos, $radius, \$angle, $wcoord, $wsize );
              }
            );

*point = \&CIAO::Lib::Region::Shape::point;
*angle  = \&CIAO::Lib::Region::Shape::angle;
*sides  = \&CIAO::Lib::Region::Shape::radii;

ffi->attach( [ regCopyBox       => 'copy'   ], [ 'Box_t' ] => 'Box_t' );
ffi->attach( [ regCalcAreaBox   => 'area'   ], [ 'Box_t' ] =>  'double' );
ffi->attach( [ regCalcExtentBox => 'extent' ], [ 'Box_t', 'double[2]', 'double[2]' ] => 'int' );
ffi->attach( [ regInsideBox     => 'inside' ], [ 'Box_t', 'double', 'double' ] => 'bool' );

1;
