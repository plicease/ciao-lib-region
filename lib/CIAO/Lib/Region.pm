package CIAO::Lib::Region;

# ABSTRACT: a really awesome library

use strict;
use warnings;

our $VERSION = '0.01';

use FFI::Platypus;
use Alien::LibCIAORegion;

use constant { Exclude => 0, Include => 1};
use constant { regAND => 0, regOr => 1 };
use constant { Unknown => 0, Logical => 1, Physical => 2, World => 3 };

use constant ffi => FFI::Platypus->new( api => 1,
                              lib => [ Alien::LibCIAORegion->dynamic_libs ],
                             );

ffi->type('object(CIAO::Lib::Region)'  => 'region_t' );
ffi->type('object(CIAO::Lib::Region::Shape)'   => 'shape_t' );

ffi->custom_type( 'heap_string' =>
                   {
                    native_type => 'opaque',
                    native_to_perl => sub {
                        my($ptr) = @_;
                        my $str = ffi->cast( 'opaque' => 'string', $ptr ); # copies the string
                        free($ptr);
                        $str;
                    }
                   }
                 );

sub new {
    my $class = shift;
    return @_ ? parse_( shift ) : new_( );
}

# Region Create/Free

ffi->attach( [ regCreateEmptyRegion => 'new_'       ] => ['string'  ]  => 'region_t' );
ffi->attach( [ regCopyRegion        => 'copy'       ] => ['region_t']  => 'region_t' );
ffi->attach( [ regParse             => 'parse_'     ] => ['string'  ]  => 'region_t' );
ffi->attach( [ regFree              => 'DESTROY'    ] => ['region_t']  => 'void'     );

# Region Examination

ffi->attach( [ regCompareRegion     => 'compare'    ] => ['region_t', 'region_t' ]        => 'bool'    );
ffi->attach( [ regInsideRegion      => 'inside'     ] => ['region_t', 'double', 'double'] => 'bool'    );
ffi->attach( [ regGetMaxPoints      => 'max_points' ] => ['region_t' ]                    => 'long'    );
ffi->attach( [ regGetShapeNo        => 'shape'      ] => ['region_t', 'long' ]            => 'shape_t' );
ffi->attach( [ regGetNoShapes       => 'num_shapes' ] => ['region_t' ]                    => 'long'    );
ffi->attach( [ regOverlapRegion     => 'overlap'    ] => ['region_t', 'region_t']         => 'int'     );

# Region Computation

ffi->attach( [ regArea                  => 'area'       ] => ['region_t', 'double[2]', 'double[2]', 'double']                 => 'double' );
ffi->attach( [ regComputePixellatedArea => 'pixel_area' ] => ['region_t', 'double[2]', 'double[2]', 'double']                 => 'double' );
ffi->attach( [ regExtent                => 'extent'     ] => ['region_t', 'double[2]', 'double[2]', 'double[2]', 'double[2]'] => 'int'    );
ffi->attach( [ regGetRegionBounds       => 'bounds'     ] => ['region_t', 'double[2]', 'double[2]']                           => 'void'   );

ffi->attach( [ regAppendShape => 'append_shape' ] => [ 'region_t', 'string',
                                                        'int', 'int',
                                                        'double[]', 'double[]',
                                                        'long',
                                                        'double[]', 'double[]',
                                                        'int', 'int'] => 'void' );

# Region Conversion

#ffi->attach( [ regConvertWorldRegion => '' ] => [ 'region_t', 'double', 'regInvertFunction invert'] => 'void' );
#ffi->attach( [ regConvertRegion => '' ] => ['regRegion* Region', 'double scale', 'regInvertFunction invert, int force'] => 'void' );

ffi->attach( [ regInvert       => 'invert'        ] => [ 'region_t' ], 'region_t' );
#ffi->attach( [ regRegionToList => 'to_list'       ] => [ 'region_t', 'double', 'double', 'double', 'double', 'double', 'double**', 'double**', 'long*'] => 'int' );
#ffi->attach( [ regRegionToMask => 'to_mask'       ] => [ 'region_t', 'double', 'double', 'double', 'double', 'double', 'short **', 'long *', 'long *'] => 'int' );
ffi->attach( [ regResolveField => 'resolve_field' ] => [ 'region_t', 'double[2]', 'double[2]'] => 'void' );


# Region Logical Combination

ffi->attach( [ regCombineRegion   => 'combine'   ] => ['region_t', 'region_t'], 'region_t' );
ffi->attach( [ regUnionRegion     => 'union'     ] => ['region_t', 'region_t'], 'region_t' );
ffi->attach( [ regIntersectRegion => 'intersect' ] => ['region_t', 'region_t'], 'region_t' );

# Region Ascii Parsing

ffi->attach( [ regReadAsciiRegion  => 'read_ascii'   ] => ['string', 'int' ]                        => 'region_t' );
# don't support names
ffi->attach( [ regWriteAsciiRegion => 'write_ascii_' ] => ['string', 'region_t', 'opaque', 'long']  => 'int',
              sub {
                  my ( $sub, $region, $filename  ) = @_;
                  $sub->( $filename, $region, undef, 0 );
                  }
            );

# Region printing

ffi->attach( [ regPrintRegion => 'print' ] => [ 'region_t'] => 'void' );
ffi->attach( [ regToStringRegion => 'to_string' ] => ['region_t' ] => 'heap_string' );

# not needed; use to_string
# ffi->attach( [ regComposeRegion => 'compose' ] => [ 'region_t', 'string', 'long'] => 'void');
# deprecated
# ffi->attach( [ regAllocComposeRegion => 'alloc_compose_region' ] => ['region_t'] => 'string');


1;

# COPYRIGHT

__END__


=head1 SYNOPSIS


=head1 SEE ALSO

